@echo off
@echo 请输入项目路径
set /p gitDir=
@echo 请输入版本号min
set /p oldVersion=
@echo 请输入版本号Max
set /p nowVersion=
@echo 请输入补丁存放路径
set /p patchDir=
@echo 请输入源码包
set /p sourceDir=
@echo 项目路径:%gitDir%
@echo 版本号min:%oldVersion%
@echo 版本号Max:%nowVersion%
@echo 补丁存放路径:%patchDir%
@echo 源码包:%sourceDir%
@pause 请输入任意字符开始打包
java -jar ./hutool-db-test-1.0-SNAPSHOT.jar %gitDir% %oldVersion% %nowVersion% %patchDir% %sourceDir%
@pause 请输入任意字符结束